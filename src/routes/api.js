const express = require("express");
const router = express.Router();
const AuthController = require("../controllers/AuthController");
const ProductController = require("../controllers/ProductController");
const middleWares = require("../middlewares");

// [POST] /api/login
router.post("/login", AuthController.loginHandler);

// [POST] /api/signup
router.post("/signup", AuthController.signupHandler);

// [POST] /api/products/create-product
router.post(
  "/products/create-product",
  middleWares.auth,
  middleWares.checkAuth,
  ProductController.createProduct
);

// [GET] /api/products/get-all-products?category
router.get(
  "/products/get-all-products",
  middleWares.auth,
  middleWares.checkAuth,
  ProductController.getAllProducts
);

// [PUT] /api/products/update-product
router.put(
  "/products/update-product",
  middleWares.auth,
  middleWares.checkAuth,
  ProductController.updateProduct
);

// [DELETE] /api/products/remove-product?id
router.delete(
  "/products/remove-product",
  middleWares.auth,
  middleWares.checkAuth,
  ProductController.removeProduct
);

module.exports = router;

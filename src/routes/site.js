const express = require("express");
const router = express.Router();
const SiteController = require("../controllers/SiteController");

// [GET] /product?id
router.get("/product", SiteController.renderProductPage);

// [GET] /login
router.get("/login", SiteController.renderLoginPage);

// [GET] /logout
router.get("/logout", SiteController.logout);

// [GET] /signup
router.get("/signup", SiteController.renderSignupPage);

// [GET] /:slug
router.get("/:slug", SiteController.notFoundPage);

// [GET] /
router.get("/", SiteController.renderHomePage);

module.exports = router;

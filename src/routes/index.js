const siteRouter = require("./site");
const apiRouter = require("./api");
const middleWares = require("../middlewares");

const init = (app) => {
  app.use("/api", apiRouter);
  app.use("/", middleWares.auth, siteRouter);
};

module.exports = { init };

const express = require("express");
const path = require("path");
const session = require("express-session");
const cookieParser = require("cookie-parser");

const config = (app) => {
  app.use(
    session({
      secret: process.env.SECRET_KEY,
      resave: false,
      saveUninitialized: true,
      cookie: { secure: false },
    })
  );
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, "../public")));
  app.use(express.json()); // for parsing application/json
  app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
  app.set("view engine", "ejs");
  app.set("views", path.join(__dirname, "../views"));
};

module.exports = { config };

"use strict";

module.exports = {
  async up(queryInterface) {
    await queryInterface.addConstraint("Products", {
      fields: ["categoryId"],
      type: "foreign key",
      name: "product_category_association",
      references: {
        table: "Categories",
        field: "id",
      },
    });
  },

  async down(queryInterface) {
    await queryInterface.removeConstraint(
      "Products",
      "product_category_association"
    );
  },
};

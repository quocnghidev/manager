const ResponseData = require("../utils/ResponseData");
const EnumStatus = require("../utils/EnumStatus");
const { Product, Sequelize } = require("../models");

class ProductController {
  // [POST] /api/products/create-product
  async createProduct(req, res) {
    const { product_name, category_id, price, description } = req.body;
    try {
      // Case: Create product successful
      await Product.create({
        productName: product_name,
        description,
        categoryId: category_id,
        price,
      });
      res
        .status(200)
        .json(
          new ResponseData(EnumStatus.SUCCESS, "Create product successfully!")
        );
    } catch (error) {
      // Case: Failed to create product
      res
        .status(409)
        .json(new ResponseData(EnumStatus.ERROR, "Can not create product!"));
    }
  }

  // [GET] /api/products/get-all-products?category&search=
  async getAllProducts(req, res) {
    try {
      const categoryId = req.query.category;
      const search = req.query.search;

      const listAllSong = categoryId
        ? await Product.findAll({
            where: {
              categoryId,
              productName: { [Sequelize.Op.like]: `%${search}%` },
            },
            include: ["category"],
          })
        : await Product.findAll({
            where: {
              productName: { [Sequelize.Op.like]: `%${search}%` },
            },
            include: ["category"],
          });
      listAllSong.length
        ? res
            .status(200)
            .json(
              new ResponseData(
                EnumStatus.SUCCESS,
                "Get products successfully!",
                listAllSong
              )
            )
        : res
            .status(200)
            .json(
              new ResponseData(
                EnumStatus.EMPTY,
                "No results for this request!",
                listAllSong
              )
            );
    } catch (error) {
      // Case: Failed to excute query
      res
        .status(500)
        .json(
          new ResponseData(EnumStatus.ERROR, "Can not handle this request!")
        );
    }
  }

  // [PUT] /api/products/update-product
  async updateProduct(req, res) {
    const { id, product_name, category_id, price, description } = req.body;
    try {
      const product = await Product.findOne({ where: { id } });

      product.productName = product_name;
      product.categoryId = category_id;
      product.price = price;
      product.description = description;

      await product.save();

      res
        .status(200)
        .json(
          new ResponseData(EnumStatus.SUCCESS, "Update product successfully!")
        );
    } catch (error) {
      // Case: Failed to update product
      res
        .status(409)
        .json(new ResponseData(EnumStatus.ERROR, "Can not update product!"));
    }
  }

  // [DELETE] /api/products/remove-product?id
  async removeProduct(req, res) {
    const id = req.query.id;
    try {
      const product = await Product.findOne({ where: { id } });
      await product.destroy();
      res
        .status(200)
        .json(
          new ResponseData(
            EnumStatus.SUCCESS,
            `Remove product: ${product.productName} successfully!`,
            product
          )
        );
    } catch (error) {
      res
        .status(500)
        .json(
          new ResponseData(EnumStatus.ERROR, "Can not remove this product!")
        );
    }
  }
}

module.exports = new ProductController();

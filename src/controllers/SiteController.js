const { Product, Category } = require("../models");

class SiteController {
  // [GET] /
  async renderHomePage(req, res) {
    // Case: not authenticated
    if (!res.locals.authed) {
      return res.redirect("/login");
    }

    // Case: not admin
    if (!res.locals.user.role) {
      return res.render("error", { status: 403 });
    }

    // Case: authenticated and is admin
    const categories = await Category.findAll();
    res.render("index", { categories });
  }

  // [GET] /login
  renderLoginPage(req, res) {
    // Case: Authenticated
    if (res.locals.authed) {
      return res.redirect("/");
    }

    // Case: not authenticated
    res.render("login");
  }

  // [GET] /signup
  renderSignupPage(req, res) {
    // Case: Authenticated
    if (res.locals.authed) {
      return res.redirect("/");
    }

    // Case: not authenticated
    res.render("signup");
  }

  // [GET] /logout
  logout(req, res) {
    // Case: Authenticated
    if (res.locals.authed) {
      return req.session.destroy(() => {
        res.clearCookie("accessToken");
        res.redirect("/");
      });
    }
    res.redirect("/login");
  }

  // [GET] /product?id
  async renderProductPage(req, res) {
    // Case: not authenticated
    if (!res.locals.authed) {
      return res.redirect("/login");
    }

    // Case: not admin
    if (!res.locals.user.role) {
      return res.render("error", { status: 403 });
    }

    // Case: authenticated and is admin
    const id = req.query.id;
    const product = await Product.findOne({ where: { id } });

    if (!product) {
      return res.render("error", { status: 404 });
    }

    const categories = await Category.findAll();
    res.render("product", { categories, product });
  }

  // [GET] /:slug
  notFoundPage(req, res) {
    res.render("error", { status: 404 });
  }
}

module.exports = new SiteController();

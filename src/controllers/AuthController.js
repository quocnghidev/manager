const { User } = require("../models");
const bcrypt = require("bcrypt");
const saltRounds = parseInt(process.env.SALT_ROUNDS);
const ResponseData = require("../utils/ResponseData");
const EnumStatus = require("../utils/EnumStatus");
const jwt = require("jsonwebtoken");

class ApiController {
  // [POST] /api/login
  async loginHandler(req, res) {
    const { email, password } = req.body;
    const user = await User.findOne({ where: { email } });
    // Case: Is not a valid user
    if (!user) {
      return res
        .status(404)
        .json(
          new ResponseData(
            EnumStatus.NOT_FOUND,
            "This email does not match any account!"
          )
        );
    }
    // Check password
    const correct = await bcrypt.compare(password, user.password);

    // Case: Wrong password
    if (!correct) {
      return res
        .status(401)
        .json(new ResponseData(EnumStatus.ERROR, "Wrong password!"));
    }

    // Case: Right password
    jwt.sign(
      { id: user.id, name: user.name, email: user.email, role: user.role },
      process.env.SECRET_KEY,
      (err, token) => {
        res
          .status(200)
          .cookie("accessToken", token, {
            expires: new Date(Date.now() + 2 * 3600000),
            httpOnly: true,
          })
          .json(new ResponseData(EnumStatus.SUCCESS, "Login successfully!"));
      }
    );
  }

  // [POST] /api/signup
  async signupHandler(req, res) {
    let { name, email, password } = req.body;

    // Hashing user password
    password = await bcrypt.hash(password, saltRounds);
    try {
      // Case: Create user successful
      await User.create({ name, email, password });
      res
        .status(200)
        .json(
          new ResponseData(EnumStatus.SUCCESS, "Create account successfully!")
        );
    } catch (error) {
      // Case: Failed to create user
      res
        .status(409)
        .json(new ResponseData(EnumStatus.ERROR, "Can not create account!"));
    }
  }
}

module.exports = new ApiController();

const express = require("express");
const env = require("dotenv");
const viewEngine = require("./config/viewEngine");
const routes = require("./routes");
const app = express();

env.config();
viewEngine.config(app);
routes.init(app);

const server = app.listen(process.env.PORT || 3000, () => {
  console.log(`Listening on port ${server.address().port}`);
});

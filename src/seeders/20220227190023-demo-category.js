"use strict";

module.exports = {
  async up(queryInterface) {
    await queryInterface.bulkInsert("Categories", [
      {
        title: "Smart Phone",
        description: "Find and buy your interested Smart Phone",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: "Notebook",
        description: "Find and buy your interested Notebook",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: "Tablet",
        description: "Find and buy your interested Tablet",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface) {
    await queryInterface.bulkDelete("Products", null, {});
    await queryInterface.bulkDelete("Categories", null, {});
  },
};

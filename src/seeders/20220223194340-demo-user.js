"use strict";

const bcrypt = require("bcrypt");
const saltRounds = parseInt(process.env.SALT_ROUNDS);

module.exports = {
  async up(queryInterface) {
    await queryInterface.bulkInsert("Users", [
      {
        name: "Lã Quốc Nghị",
        email: "admin@manager.com",
        password: await bcrypt.hash("admin", saltRounds),
        role: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Cao Hoài Nam",
        email: "caonam123@gmail.com",
        password: await bcrypt.hash("caonam123", saltRounds),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Nguyễn Đức Long",
        email: "longcua2k@gmail.com",
        password: await bcrypt.hash("longcua2k", saltRounds),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface) {
    await queryInterface.bulkDelete("Users", null, {});
  },
};

const jwt = require("jsonwebtoken");
const ResponseData = require("../utils/ResponseData");
const EnumStatus = require("../utils/EnumStatus");

class MiddleWare {
  static auth(req, res, next) {
    if (!req.cookies.accessToken) {
      res.locals.authed = false;
      return next();
    }
    try {
      const user = jwt.verify(req.cookies.accessToken, process.env.SECRET_KEY);
      res.locals.authed = true;
      res.locals.user = {
        id: user.id,
        name: user.name,
        email: user.email,
        role: user.role,
      };
      next();
    } catch (error) {
      res.locals.authed = false;
      next();
    }
  }

  static checkAuth(req, res, next) {
    // Case: Not authorized to call api
    if (!res.locals.authed) {
      return res
        .status(401)
        .json(
          new ResponseData(
            EnumStatus.NOT_AUTHORIZED,
            "Looks like you have not been authorized yet"
          )
        );
    }

    // Case: Do not have permission to call api
    if (!res.locals.user.role) {
      return res
        .status(403)
        .json(
          new ResponseData(
            EnumStatus.NOT_FORBIDDEN,
            "Looks like you do not have permission"
          )
        );
    }

    next();
  }
}

module.exports = MiddleWare;

/* global Swal */
(function ($, window, document) {
  $(function () {
    // The DOM is ready!
    const signUpForm = $("#signup-form");
    const loginForm = $("#login-form");
    const createProductForm = $("#create-product-form");
    const updateProductForm = $("#update-product-form");
    const mainTable = $("tbody");
    const categorySelect = $("#category-select-combobox");
    const searchBox = $("#main-search");

    // Category combobox select change handle
    categorySelect.on("change", function () {
      loadProductData($(this).val());
    });

    // Submit Sign Up form handler
    signUpForm.on("submit", function (event) {
      event.preventDefault();
      const body = getFormBody($(this));
      signUp(body)
        .done(function (res) {
          Swal.fire({
            title: res.status,
            text: res.message,
            icon: "success",
          }).then((result) => {
            if (result.isConfirmed) {
              window.location.href = "/login";
            }
          });
        })
        .fail(function (res) {
          handleError(res);
        });
    });

    // Submit Login form handler
    loginForm.on("submit", function (event) {
      event.preventDefault();
      const body = getFormBody($(this));
      login(body)
        .done(function (res) {
          Swal.fire({
            title: res.status,
            text: res.message,
            icon: "success",
          }).then((result) => {
            if (result.isConfirmed) {
              window.location.href = "/";
            }
          });
        })
        .fail(function (res) {
          handleError(res);
        });
    });

    // Submit update product form handler
    updateProductForm.on("submit", function (event) {
      event.preventDefault();
      const body = getFormBody($(this));
      const id = $(this).data("productId");
      const description = $(".ql-editor").html();
      body.id = id;
      body.description = description;
      Swal.fire({
        title: "WARNING",
        html: `<b>Are you sure you want to continue?</b>`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#ffc107",
        confirmButtonText: "Yes, UPDATE!",
      }).then((result) => {
        if (result.isConfirmed) {
          updateProduct(body)
            .done(function (res) {
              Swal.fire({
                title: res.status,
                text: res.message,
                icon: "success",
              }).then((result) => {
                if (result.isConfirmed) {
                  window.location.href = "/";
                }
              });
            })
            .fail(function (res) {
              handleError(res);
            });
        }
      });
    });

    // Submit create product form handler
    createProductForm.on("submit", function (event) {
      event.preventDefault();
      const body = getFormBody($(this));
      const description = $(".ql-editor").html();
      body.description = description;
      const _this = $(this);
      createProduct(body)
        .done(function (res) {
          Swal.fire({
            title: res.status,
            text: res.message,
            icon: "success",
          }).then((result) => {
            if (result.isConfirmed) {
              loadProductData(categorySelect.val());
              clearForm(_this);
            }
          });
        })
        .fail(function (res) {
          handleError(res);
        });
    });

    // Handle searching
    searchBox.on("change", function () {
      loadProductData(categorySelect.val(), $(this).val());
    });
    searchBox.closest("form").on("submit", function (event) {
      event.preventDefault();
      searchBox.trigger("change");
    });

    // Get body from <form>
    function getFormBody(form) {
      const body = {};
      const fields = form.serializeArray();
      fields.forEach((field) => {
        body[field.name] = field.value;
      });
      return body;
    }

    // Ajax call [POST] /api/signup
    function signUp(body) {
      return $.ajax({
        type: "POST",
        url: "/api/signup",
        data: body,
      });
    }

    // Ajax call [POST] /api/login
    function login(body) {
      return $.ajax({
        type: "POST",
        url: "/api/login",
        data: body,
      });
    }

    // Ajax call [POST] /api/products/create-product
    function createProduct(body) {
      return $.ajax({
        type: "POST",
        url: "/api/products/create-product",
        data: body,
      });
    }

    // Ajax call [PUT] /api/products/update-product
    function updateProduct(body) {
      return $.ajax({
        type: "PUT",
        url: "/api/products/update-product",
        data: body,
      });
    }

    // Ajax call [GET] /api/products/get-all-products?category
    function getAllProduct(categoryId, search) {
      const url = `/api/products/get-all-products?category=${
        categoryId == 0 || !categoryId ? "" : categoryId
      }&search=${!search ? "" : search}`;
      return $.ajax({
        type: "GET",
        url,
      });
    }

    // Ajax call [DELETE] /api/products/remove-product?id
    function removeProduct(id) {
      const url = `/api/products/remove-product?id=${id}`;
      return $.ajax({
        type: "DELETE",
        url,
      });
    }

    // format date data
    function formatDate(date) {
      date = new Date(date);
      return date.toLocaleString("vi-VN", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit",
      });
    }

    function loadProductData(categoryId, search) {
      getAllProduct(categoryId, search)
        .done(function (res) {
          let html = "";
          $.each(res.data, function (index, val) {
            html += `<tr data-product-id="${val.id}"
                         data-product-name="${val.productName}"
                         class="table__row">
                        <td class="table__td">
                            <div class="table__checkbox table__checkbox--all">
                                <label class="checkbox">
                                    <input type="checkbox" data-checkbox="product">
                                      <span class="checkbox__marker">
                                        <span class="checkbox__marker-icon">
                                            <svg class="icon-icon-checked">
                                                <use xlink:href="#icon-checked"></use>
                                            </svg>
                                        </span>
                                      </span>
                                </label>
                            </div>
                        </td>
                        <td class="d-none d-lg-table-cell table__td">
                          <span class="text-grey">#${val.id}</span>
                        </td>
                        <td class="table__td">${val.productName}</td>
                        <td class="table__td">
                          <span class="text-grey">
                          ${val.category.title}
                          </span>
                        </td>
                        <td class="table__td">
                          <span>$${val.price}</span>
                        </td>
                        <td class="d-none d-lg-table-cell table__td">
                          <span class="text-grey">
                          ${formatDate(val.createdAt)}
                          </span>
                        </td>
                        <td class="table__td table__actions">
                            <div class="items-more">
                                <button class="items-more__button">
                                    <svg class="icon-icon-more">
                                        <use xlink:href="#icon-more"></use>
                                    </svg>
                                </button>
                                <div class="dropdown-items dropdown-items--right${
                                  index >= 6 ? " dropdown-items--up" : ""
                                }">
                                    <div class="dropdown-items__container">
                                        <ul class="dropdown-items__list">
                                            <li class="dropdown-items__item">
                                              <a class="dropdown-items__link">
                                                <span class="dropdown-items__link-icon">
                                                  <svg class="icon-icon-view">
                                                    <use xlink:href="#icon-view"></use>
                                                  </svg>
                                                </span>
                                                Details
                                              </a>
                                            </li>
                                            <li class="dropdown-items__item">
                                              <a class="dropdown-items__link">
                                                <span class="dropdown-items__link-icon">
                                                  <svg class="icon-icon-trash">
                                                    <use xlink:href="#icon-trash"></use>
                                                  </svg>
                                                </span>
                                                Delete
                                              </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>`;
          });

          mainTable.empty();
          mainTable.append(html);

          //Handle detail button
          const btnViews = $(".icon-icon-view").closest("li");
          btnViews.on("click", function () {
            const row = $(this).closest("tr");
            const id = row.data("productId");
            viewProductHandler(id);
          });

          // Handle remove button
          const btnRemoves = $(".icon-icon-trash").closest("li");
          btnRemoves.on("click", function () {
            const row = $(this).closest("tr");
            const id = row.data("productId");
            const productName = row.data("productName");
            removeProductHandler(id, productName);
          });

          // Fix UI FE
          let sub = 10 - res.data.length;
          sub = sub >= 0 ? sub : 0;
          html = "";
          for (let i = 0; i < sub; i++) {
            html += '<tr class="table__row"></tr>';
          }

          mainTable.append(html);
        })
        .fail(function (res) {
          handleError(res);
        });
    }

    // Handle view product button click
    function viewProductHandler(id) {
      window.location.href = `/product?id=${id}`;
    }

    // Handle remove product button click
    function removeProductHandler(id, productName) {
      Swal.fire({
        title: "WARNING",
        html: `<b>Do you really want to remove ${productName}?</b>`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dc3545",
        confirmButtonText: "Yes, DELETE!",
      }).then((result) => {
        if (result.isConfirmed) {
          removeProduct(id)
            .done(function (res) {
              Swal.fire({
                title: res.status,
                text: res.message,
                icon: "success",
              }).then((result) => {
                if (result.isConfirmed) {
                  loadProductData(categorySelect.val());
                }
              });
            })
            .fail(function (res) {
              handleError(res);
            });
        }
      });
    }

    // Show message when catch error
    function handleError(res) {
      const error = res.responseJSON;
      Swal.fire({
        title: error.status,
        text: error.message,
        icon: "error",
      });
    }

    // Clear Form
    function clearForm(form) {
      const description = form.find(".ql-editor");
      description.html("");
      form[0].reset();
    }

    // Load data for first time visit
    if (categorySelect.length) {
      loadProductData(categorySelect.val());
    }
  });
})(window.jQuery, window, document);
